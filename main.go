package main

import (
	"fmt"
	"i3blocks-battery/battery"
	"strconv"
)

func main() {
	currentPercentage := battery.GetBatteryPercentage()

	lastPercentage := battery.LoadFromFile()

	if lastPercentage > currentPercentage {
		if currentPercentage == 30 || currentPercentage == 20 || currentPercentage == 10 {
			battery.ShowNotify(currentPercentage)
		}
	}

	fmt.Print(strconv.Itoa(currentPercentage) + "%")

	battery.SaveToFile(currentPercentage)
}
