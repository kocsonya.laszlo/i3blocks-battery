package battery

import (
	"fmt"
	"github.com/martinlindhe/notify"
	"log"
	"strings"
)

import (
	"io/ioutil"
	"os/exec"
	"regexp"
	"strconv"
)

func GetBatteryPercentage() int {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	cmd := exec.Command("acpi", "-b")
	out, err := cmd.Output()
	if err != nil {
		log.Fatal("cannot run acpi command")
	}

	outStr := string(out[:])

	regexObj, err := regexp.Compile(`\d+%`)
	if err != nil {
		log.Fatal(err)
	}
	found := "0"
	matches := regexObj.FindAllString(outStr,2)
	if nil != matches {
		for _, v := range matches {
			if v != "0%" {
				found = strings.Replace(v, "%", "", 1)
			}
		}
	}

	ret, _ := strconv.Atoi(found)

	return ret
}

func SaveToFile(percentage int) {
	err := ioutil.WriteFile("/tmp/battery", []byte(strconv.Itoa(percentage)), 0644)
	if err != nil {
		log.Fatal(err)
	}
}

func LoadFromFile() int {
	out, err := ioutil.ReadFile("/tmp/battery")
	if err != nil {
		_ = ioutil.WriteFile("/tmp/battery", []byte("100"), 0644)
		out = []byte("100")
	}

	i, err := strconv.Atoi(string(out))
	if err != nil {
		log.Fatal(err)
	}

	return i
}

func ShowNotify(currentPercentage int) {
	fmt.Println("notif")
	notify.Alert("Go Battery", "Battery warning", "Your battery is only at "+strconv.Itoa(currentPercentage)+"%", "")
}
