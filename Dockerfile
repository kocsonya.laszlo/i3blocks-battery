FROM golang:1.14.3

RUN apt update && \
    apt install -y libnotify-dev && \
    go get "github.com/martinlindhe/notify"

CMD go build -o /go/src/i3blocks-battery/main src/i3blocks-battery/main.go
